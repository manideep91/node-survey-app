var express     = require('express');
var bodyParser  = require('body-parser');
var logger      = require('morgan');
var mongoose    = require('mongoose');
var cool        = require('cool-ascii-faces');

//database configuration
var dbConfig=require('./server/config/db');
mongoose.connect(dbConfig.url);


var app=express();
//middlewares
app.use(logger('dev'));
app.use(bodyParser.json());

app.get('/',function(req,res){
	res.sendFile(__dirname+'/client/views/survey_list.html')
});

app.use('/js',express.static(__dirname+'/client/js'));
app.use('/css',express.static(__dirname+'/client/css'));

//routers
var routes=require('./server/routes/index');
app.use('/',routes);

var routes=require('./server/routes/index');
app.use('/test',routes);


app.get('/cool', function(request, response) {
  response.send(cool());
});


app.use(function (req, res, next) {

    // Website you wish to allow to connect
   // res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//use environment defined port or 3000
var port=process.env.PORT||3000;
//listen on port
app.listen(port);
console.log('The survey app is running on port '+port);