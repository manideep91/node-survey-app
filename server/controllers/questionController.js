var Question=require('../models/question');
var mongoose = require('mongoose');
var QuestionResponse=require('../models/questionResponse');
var Schema   = mongoose.Schema;
var db = mongoose.connection;
var Users=require('../models/cna');

//function to save survey.
module.exports.createQuestion=function(req,res){
    res.set('Access-Control-Allow-Origin', '*');
    var question=new Question(req.body);
    for(var i=0;i<req.body.options.length;i++){
        question.options[i].resultCount=0;    
    }
    
    question.save(function(err,result){
       if(err){
           console.log(err);
           res.send(err);
       } 
       res.json(result);
    });
}

//functio to get all questions irrespective of survey
module.exports.listAll=function(req,res){
    res.set('Access-Control-Allow-Origin', '*');
    Question.find({},function(err,results) {
        if(err){
            console.log(err);
            res.send(err);
        }else{
            res.json(results);
        }
    })
}

//function to get all the questions related to a specific survey
module.exports.getQuestionsOfSurvey=function(req,res){
    res.set('Access-Control-Allow-Origin', '*');
    console.log(req.body.surveyID);
    Question.find({"survey":req.body.surveyID},function(err,results){
        if(err){
            res.send(err);
        }else{
            res.json(results);
        }
        
    })
}

//function used to update a survey object and return the updated object
module.exports.updateQuestion=function(req,res){
    res.set('Access-Control-Allow-Origin', '*');
    var question=new Question(req.body);
    Question.findByIdAndUpdate(req.body._id,question,{new: true},function(err,result){
        if(err){
            res.send(err);
        }else{
            res.json(result);
        }
    });
}

//function to capture the response of the question send by user
module.exports.updateResponseOfQuestion=function(req,res){
    res.set('Access-Control-Allow-Origin', '*');
     var question=new QuestionResponse(req.body);
    
        Question.findOne({"_id":question.questionId}).exec(function(err, doc) {
        var convertedJSON = JSON.parse(JSON.stringify(doc));
        var questionUpdate=new Question(convertedJSON);
        //check for options and increment the count by 1.
       for (var j = 0; j <questionUpdate.options.length; j++){
           if(questionUpdate.options[j]._id==question.selectedOptions[0].optionId)
           questionUpdate.options[j].resultCount=questionUpdate.options[j].resultCount+1;
        }
           console.log(questionUpdate.options[0].resultCount);
           //update the question response
          Question.findByIdAndUpdate(questionUpdate._id,questionUpdate,{new: true},function(err,result){
        if(err){
            res.send(err);
        }else{
            res.json(result);
        }
    });
        
    });
}