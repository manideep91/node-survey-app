var Survey = require('../models/survey');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var db = mongoose.connection;

//function to save survey.
module.exports.createSurvey = function (req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    var survey = new Survey(req.body);
    survey.save(function (err, result) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        res.json(result);
    });
}

//function used to get all the surveys.
module.exports.listAll = function (req, res) {
    
    console.log("In api call of get all surveys");
    res.set('Access-Control-Allow-Origin', '*');
    Survey.find({}, function (err, results) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(results);
            res.json(results);
        }
    })
}

//function used to update a survey object and return the updated object
module.exports.updateSurvey = function (req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    var survey = new Survey(req.body);
    Survey.findByIdAndUpdate(req.body._id, survey, { new: true }, function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.json(result);
        }
    });
}