var User=require('../models/user');
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var db = mongoose.connection;

//function to store user.
module.exports.createUser=function(req,res){
    res.set('Access-Control-Allow-Origin', '*');
    console.log(req.body);
    var user=new User(req.body);
    user.save(function(err,result){
       if(err){
           console.log(err);
           res.send(err);
       } 
       res.json(result);
       
    });
}

//function used to get all the users.
module.exports.listAll = function (req, res) {
    
    console.log("In api call of get all users in DB");
    res.set('Access-Control-Allow-Origin', '*');
    User.find({}, function (err, results) {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            console.log(results);
            res.json(results);
        }
    })
}

//function to check if user is present in DB

module.exports.validateUser = function (req, res) {
    
   res.set('Access-Control-Allow-Origin', '*');
    console.log(req.body.email);
    console.log(req.body.password);
     
      User.find({"email":req.body.email,"password":req.body.password},function(err,results){
        if(err){
            res.send(err);
        }else{
            res.json(results);
        }
        
    })
}
