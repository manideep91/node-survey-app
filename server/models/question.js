var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var db = mongoose.connection; 
var Survey = mongoose.model('survey', require('../models/survey'));  

//define a question schema
var questionSchema=new Schema({
   question:String,
   creationDate:Date,
   type:String,
   survey:{type:Schema.Types.ObjectId,ref:Survey},
   options:[{
       option:String,
       resultCount:Number
   }] 
});

var question=mongoose.model('question',questionSchema);
module.exports=question;
