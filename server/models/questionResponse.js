var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
//define a question schema
var questionResponseSchema=new Schema({
   questionId:String,
 selectedOptions:[
     {
         optionId:String
     }
 ]
});

var question=mongoose.model('questionResponse',questionResponseSchema);
module.exports=question;