var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var db = mongoose.connection; 

//define survey schema
var surveySchema=new Schema({
    title:String,
    category:String,
    isPublished:Boolean,
    publishedDate:Date,
    creationDate:Date
});
var survey=mongoose.model('survey',surveySchema);
module.exports=survey;