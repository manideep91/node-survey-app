var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var db = mongoose.connection; 
//define User schema
var userSchema=new Schema({
    email:{type:String,unique:true},
    password:String,
    gender:String,
    age:Number,
    role:String
});

var user=mongoose.model('User',userSchema);
module.exports=user;