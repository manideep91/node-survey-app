var express      = require('express');
var router       = express();
var userCtrl     = require('../controllers/userController');
var surveyCtrl   = require('../controllers/surveyController');
var questionCtrl = require('../controllers/questionController');
var user=require('../models/cna');

//this is a basic api used to test the rest api
router.get('/api',function(req,res){
   res.send('API set up is working'); 
});

//API to save an user into Databse 
router.post('/api/createUser',userCtrl.createUser);
router.get('/api/getAllUsers',userCtrl.listAll);
router.post('/api/validateUser',userCtrl.validateUser);




//API to save a survey into Databse
router.post('/api/createSurvey',surveyCtrl.createSurvey);
//API to get update the survey
router.put('/api/updateSurvey',surveyCtrl.updateSurvey);
//API to get all the surveys from database
router.get('/api/getAllSurveys',surveyCtrl.listAll);



//API to save a question related to survey
router.post('/api/createQuestion',questionCtrl.createQuestion);
//API to get all the questions irrespective of survey
router.get('/api/getAllQuestions',questionCtrl.listAll);
//API to get all the questions related to a survey
router.post('/api/getQuestionsOfSurvey',questionCtrl.getQuestionsOfSurvey);
//API to update the question that used has responded second time
router.put('/api/updateQuestion',questionCtrl.updateQuestion);
//API to update the response of question send by user
router.put('/api/updateQuestionResponse',questionCtrl.updateResponseOfQuestion);



//Demo API to test sorting functionality
router.get('/api/testSort',user.getAll);
module.exports=router;